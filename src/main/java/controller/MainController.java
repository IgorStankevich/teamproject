package controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class MainController {
    @FXML
    private TextField pathTxtFld;

    @FXML
    private void chooseFile(){
        FileChooser chooser = new FileChooser();
        configureFileChooser(chooser);
        File file = chooser.showOpenDialog(new Stage());
        if (file != null) {
            pathTxtFld.setText(file.getPath());
        }
    }

    private void configureFileChooser(FileChooser chooser) {
        chooser.setTitle("Choose file for import");
        chooser.setInitialDirectory(new File(System.getProperty("user.home")));
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Files", "*.*"),
                new FileChooser.ExtensionFilter("DBF", "*.dbf"),
                new FileChooser.ExtensionFilter("CSV", "*.csv"),
                new FileChooser.ExtensionFilter("DLM", "*.dlm"),
                new FileChooser.ExtensionFilter("XLS", "*.xls")
        );
    }

    @FXML
    private void exit(){
        System.exit(0);
    }
}
